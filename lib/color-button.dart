import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class ColorButton extends StatefulWidget {
  final ColorButtonState state;
  final ColorSelectedCallback onColorSelected;

  ColorButton({String text, @required Color color, ColorSelectedCallback onColorSelected, Key key}) :
        assert(color != null),
        onColorSelected = onColorSelected,
        state = ColorButtonState(text, color),
        super(key: key);

  @override
  State<StatefulWidget> createState() => state;
}

class ColorButtonState extends State<ColorButton> {
  String text;
  Color color;

  ColorButtonState(this.text, this.color);

  void changeColor(Color color) {
    setState(() => {this.color = color});
  }

  @override
  Widget build(BuildContext context) {
    var colorButton =  context.widget as ColorButton;
    return FlatButton(
      child: text != null ? Text(text) : new Container(),
      color: this.color,
      onPressed: () {
        showDialog(
          context: context,
          child: AlertDialog(
            title: const Text('Wähle eine Farbe'),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: color,
                onColorChanged: changeColor,
                showLabel: true,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: <Widget>[
              RaisedButton(
                color: Colors.white,
                child: Text(
                  "OK",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  colorButton.onColorSelected?.call(this.color);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

typedef ColorSelectedCallback = void Function(Color color);