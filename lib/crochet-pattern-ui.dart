import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';
import 'color-button.dart';
import 'crochet-pattern-game.dart';

class CrochetPatternUI extends StatefulWidget {
  final CrochetPatternUIState state;

  CrochetPatternUI() : state = CrochetPatternUIState();

  State<StatefulWidget> createState() => state;
}

class CrochetPatternUIState extends State<CrochetPatternUI>
    with WidgetsBindingObserver {
  final GlobalKey _gameBoundaryKey = new GlobalKey();
  final CrochetGame _game;
  final TextEditingController _rowsTextController = TextEditingController();
  final TextEditingController _columnsTextController = TextEditingController();

  UIScreen currentScreen = UIScreen.playing;

  CrochetPatternUIState() : _game = CrochetGame();

  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _rowsTextController.text = _game.getGridRows().toString();
    _columnsTextController.text = _game.getGridColumns().toString();
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _rowsTextController.dispose();
    _columnsTextController.dispose();
    super.dispose();
  }

  void update() {
    setState(() {});
  }

  Widget spacer({int size}) {
    return Expanded(
      flex: size ?? 100,
      child: Center(),
    );
  }

  Widget _gridSizeDialog(BuildContext context, Function afterSubmit) {
    _rowsTextController.text = _game.getGridRows().toString();
    _columnsTextController.text = _game.getGridColumns().toString();
    return Container(
      height: 265,
      child: Column(
        children: <Widget>[
          SizedBox(height: 10),
          Padding(
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
              child: TextFormField(
                controller: _rowsTextController,
                maxLines: 1,
                autofocus: false,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Anzahl der Reihen',
                ),
              )),
          Container(
            width: 150.0,
            height: 1.0,
            color: Colors.grey[400],
          ),
          Padding(
              padding: EdgeInsets.only(top: 10, right: 15, left: 15),
              child: TextFormField(
                controller: _columnsTextController,
                maxLines: 1,
                autofocus: false,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Anzahl der Spalten',
                ),
              )),
          SizedBox(height: 50),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FlatButton(
                color: Colors.grey,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  "Abbrechen",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(width: 8),
              FlatButton(
                color: Colors.white,
                child: Text(
                  "OK",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onPressed: () {
                  _game.setGridSize(int.parse(_rowsTextController.text),
                      int.parse(_columnsTextController.text));
                  Navigator.of(context).pop();
                  afterSubmit?.call();
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          toolbarHeight: 35,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: shareCrochet,
                  child: Icon(Icons.share_rounded),
                )),
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: _game.optimizeViewPort,
                  child: Icon(
                    Icons.branding_watermark_outlined,
                  ),
                )),
          ]),
      drawer: Drawer(
          child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Builder(builder: (BuildContext context) {
                return ColorButton(
                    text: "Farbe 1",
                    color: _game.getGridRowColor(),
                    onColorSelected: (Color color) {
                      _game.setGridRowColor(color);
                      Navigator.pop(context);
                    });
              })),
          Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Builder(builder: (BuildContext context) {
                return ColorButton(
                    text: "Farbe 2",
                    color: _game.getGridOddRowColor(),
                    onColorSelected: (Color color) {
                      _game.setGridOddRowColor(color);
                      Navigator.pop(context);
                    });
              })),
          Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: FlatButton(
                color: Colors.grey,
                child: Text("Gittergröße"),
                onPressed: () {
                  showDialog(
                      context: context,
                      child: AlertDialog(
                        title: Text("Gittergröße"),
                        content: _gridSizeDialog(context, () {
                          Navigator.pop(context);
                        }),
                      ));
                },
              )),
          CheckboxListTile(
            title: Text("Marker anzeigen"),
            value: !_game.isGridRectMarkerDisabled,
            onChanged: (newValue) {
              setState(() {
                _game.gridRectMarkerDisabled = !newValue;
              });
            },
            controlAffinity:
                ListTileControlAffinity.leading, //  <-- leading Checkbox
          ),
          ListTile(
            onTap: () => {},
          )
        ],
      )),
      body: Stack(
        children: [
          Container(
              child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onLongPressStart: _game.onLongPressStart,
            onLongPressMoveUpdate: _game.onLongPressMoveUpdate,
            onLongPressEnd: _game.onLongPressEnd,
            // TODO: Check why ScaleDetector mixin is not recognized
            onScaleStart: _game.onScaleStart,
            onScaleUpdate: _game.onScaleUpdate,
            onScaleEnd: _game.onScaleEnd,
            child: RepaintBoundary(
              key: _gameBoundaryKey,
              child: _game.widget,
            ),
          )),
        ],
      ),
    );
  }

  void shareCrochet({Function callBack}) async {
    RenderRepaintBoundary boundary =
        _gameBoundaryKey.currentContext.findRenderObject();
    var image = await boundary.toImage(pixelRatio: 5.0);
    ByteData bytes = await image.toByteData(format: ImageByteFormat.png);
    await WcFlutterShare.share(
        sharePopupTitle: 'Share Crochet',
        fileName: 'crochet.png',
        mimeType: 'image/png',
        bytesOfFile: bytes.buffer.asUint8List());
    callBack?.call();
  }
}

enum UIScreen { playing, settings }
