import 'dart:developer';
import 'dart:math' as math;
import 'dart:ui';

import 'package:crochet_pattern/zoom.dart';
import 'package:flame/components/component.dart';
import 'package:flame/components/composed_component.dart';
import 'package:flame/components/mixins/has_game_ref.dart';
import 'package:flame/components/mixins/resizable.dart';
import 'package:flame/components/mixins/tapable.dart';
import 'package:flame/game/game.dart';
import 'package:flame/position.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'crochet-grid-rect.dart';
import 'crochet-pattern-game.dart';

abstract class PositionComponentWithGameRef extends PositionComponent
    implements HasGameRef<Game> {
  @override
  Game gameRef;
}

class CrochetGridComponent extends PositionComponentWithGameRef
    with Resizable, Tapable, Zoomable, ComposedComponent {
  final rectLength = 30.0;
  int _rows = 0;
  int _columns = 0;
  List<List<CrochetRectComponent>> _rects = new List();
  Color _rowColor;
  Color _oddRowColor;
  bool _rectMarkerDisabled = false;

  CrochetGridComponent(CrochetGame gameRef, int rows, int columns,
      {Color rowColor = Colors.white, Color oddRowColor = Colors.white54}) {
    this._rows = rows;
    this._columns = columns;
    this.gameRef = gameRef;
    _rowColor = rowColor;
    _oddRowColor = oddRowColor;
    _initGrid();
  }

  // TODO: copy to immutable list
  get rects => _rects;

  get isRectMarkerDisabled => _rectMarkerDisabled;

  set rectMarkerDisabled(bool disabled) {
    _rectMarkerDisabled = disabled;
    _updateRectProperties();
  }

  Color get rowColor {
    return _rowColor;
  }

  set rowColor(Color color) {
    _rowColor = color;
    _updateRectProperties();
  }

  Color get oddRowColor {
    return _oddRowColor;
  }

  set oddRowColor(Color color) {
    _oddRowColor = color;
    _updateRectProperties();
  }

  int get rows {
    return _rows;
  }

  int get columns {
    return _columns;
  }

  void setGridSize(int rows, int columns) {
    _rows = rows;
    _columns = columns;
    _initGrid();
  }

  void _initGrid() {
    components.clear();
    _rects = new List(_rows);
    [for (var i = 0; i < _rows; i += 1) i].forEach((row) {
      _rects[row] = new List<CrochetRectComponent>(_columns);
      [for (var i = 0; i < _columns; i += 1) i].forEach((column) {
        var color = row % 2 == 0 ? _rowColor : _oddRowColor;
        var size = new Size(rectLength, rectLength);
        var position = new Position(
            (rectLength * column).toDouble(), (rectLength * row).toDouble());
        var rect = new CrochetRectComponent(row, column, position, size, color)
          ..disableSelectionMarker = _rectMarkerDisabled;
        rect.onTap = this.onRectTappedDown;
        _rects[row][column] = rect;
        components.add(rect);
      });
    });
    _updateSize();
  }

  void _updateRectProperties() {
    _rects.forEach((rows) {
      rows.forEach((rect) {
        rect.initialColor = rect.row % 2 == 0 ? _rowColor : _oddRowColor;
        rect.disableSelectionMarker = _rectMarkerDisabled;
      });
    });
    _rects.forEach((rows) {
      rows.forEach((rect) {
        var rectAbove = rect.row > 0 ? _rects[rect.row - 1][rect.column] : null;
        rect.color = rectAbove != null && rectAbove.tapped
            ? rectAbove.initialColor
            : rect.initialColor;
      });
    });
  }

  void onRectTappedDown(CrochetRectComponent rectComponent) {
    if (rectComponent.row + 1 < _rects.length) {
      CrochetRectComponent rectBelow =
          _rects[rectComponent.row + 1][rectComponent.column];
      var color = rectComponent.tapped
          ? rectComponent.initialColor
          : rectBelow.initialColor;
      rectBelow.color = color;
    }
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
  }

  @override
  void handleZoomChanged(double zoomFactor) {
    _rects.forEach((rows) {
      rows.forEach((element) {
        final newSize = element.initialSize.width * zoomFactor;
        element.setByPosition(
            new Position(element.column * newSize, element.row * newSize));
        element.setBySize(new Position(newSize, newSize));
      });
    });
    _updateSize();
  }

  void _updateSize() {
    final rows = _rects.length - 1;
    final cols = _rects.isEmpty ? 0 : _rects[0].length;
    final lastRect = _rects[rows - 1][cols - 1];
    setBySize(new Position(
        lastRect.x + lastRect.width, lastRect.y + lastRect.height));
  }

  int _getFirstVisibleRow(Rect viewPort) {
    for (int rowIndex = 0; rowIndex < _rects.length; rowIndex++) {
      if (_rects[rowIndex].isEmpty) return -1;
      final rect = _rects[rowIndex][0];
      if (viewPort.contains(new Offset(viewPort.left, rect.y)) ||
          viewPort.contains(new Offset(viewPort.left, rect.y + rect.height))) {
        return rowIndex;
      }
    }
    return -1;
  }

  int _getFirstVisibleColumn(Rect viewPort) {
    if (_rects.isEmpty) return -1;
    for (int colIndex = 0; colIndex < _rects[0].length; colIndex++) {
      final rect = _rects[0][colIndex];
      if (viewPort.contains(new Offset(rect.x, viewPort.top)) ||
          viewPort.contains(new Offset(rect.x + rect.width, viewPort.top))) {
        return colIndex;
      }
    }
    return -1;
  }

  List<CrochetRectComponent> getColumnRectsInCameraViewport(Rect viewPort) {
    final columnRects = <CrochetRectComponent>[];
    var firstVisibleRow = _getFirstVisibleRow(viewPort);
    if (firstVisibleRow >= 0) {
      _rects[firstVisibleRow].forEach((columnRect) {
        if (viewPort.overlaps(columnRect.toRect())) {
          columnRects.add(columnRect);
        }
      });
    }
    return columnRects;
  }

  List<CrochetRectComponent> getRowRectsInCameraViewport(Rect viewPort) {
    final rowRects = <CrochetRectComponent>[];
    var firstVisibleColumn = _getFirstVisibleColumn(viewPort);
    if (firstVisibleColumn >= 0) {
      _rects.forEach((row) {
        final rowRect = row[firstVisibleColumn];
        if (viewPort.overlaps(rowRect.toRect())) {
          rowRects.add(rowRect);
        }
      });
    }
    return rowRects;
  }

  Size getRectSize() {
    return _rects.isEmpty || _rects[0].isEmpty
        ? Size.zero
        : new Size(_rects[0][0].width, _rects[0][0].height);
  }

  Rect computeRectSelection(Rect rawSelection) {
    rawSelection = Rect.fromLTRB(
        math.min(rawSelection.left, rawSelection.right),
        math.min(rawSelection.top, rawSelection.bottom),
        math.max(rawSelection.left, rawSelection.right),
        math.max(rawSelection.top, rawSelection.bottom));
    Rect rectSelection;
    _rects.forEach((rows) {
      rows.forEach((rect) {
        if (rawSelection.overlaps(rect.toRect())) {
          rectSelection = rectSelection == null
              ? rect.toRect()
              : rectSelection.expandToInclude(rect.toRect());
        }
      });
    });
    return rectSelection;
  }

  // TODO: return immutable representation
  List<List<CrochetRectComponent>> getRectsBySelection(Rect selection) {
    List<List<CrochetRectComponent>> selectedRects = new List();
    _rects.forEach((rows) {
      List<CrochetRectComponent> selectedRows = new List();
      rows.forEach((rect) {
        if (selection.overlaps(rect.toRect())) {
          selectedRows.add(rect);
        }
      });
      if (selectedRows.isNotEmpty) {
        selectedRects.add(selectedRows);
      }
    });
    return selectedRects;
  }

  void deleteTabbingForRects(List<List<CrochetRectComponent>> rects) {
    rects.forEach((rows) {
      rows.forEach((rect) {
        rect.tapped = false;
      });
    });
    _updateRectProperties();
  }

  void insert(List<List<bool>> rectClipBoard,
      List<List<CrochetRectComponent>> selectedRects) {
    var topLeftRect = selectedRects[0][0];
    var rowIndex = topLeftRect.row;
    rectClipBoard.forEach((rows) {
      var columnIndex = topLeftRect.column;
      if (rowIndex < _rects.length) {
        rows.forEach((rect) {
          if (columnIndex < _rects[rowIndex].length) {
            _rects[rowIndex][columnIndex].tapped = rect;
          }
          columnIndex++;
        });
      }
      rowIndex++;
    });
    _updateRectProperties();
  }

  static List<List<bool>> rectsToBool(
      List<List<CrochetRectComponent>> selectedRects) {
    List<List<bool>> booleanList = new List();
    selectedRects.forEach((rows) {
      List<bool> booleanRows = new List();
      rows.forEach((rect) {
        booleanRows.add(rect.tapped);
      });
      booleanList.add(booleanRows);
    });
    return booleanList;
  }
}
