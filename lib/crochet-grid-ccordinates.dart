import 'dart:ui';

import 'package:flame/components/component.dart';
import 'package:flame/components/mixins/has_game_ref.dart';
import 'package:flame/game/base_game.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'crochet-grid.dart';

class CrochetGridCoordinatesComponent extends Component
    implements HasGameRef<BaseGame> {
  @override
  BaseGame gameRef;
  CrochetGridComponent _grid;

  CrochetGridCoordinatesComponent(BaseGame gameRef, CrochetGridComponent grid) {
    this.gameRef = gameRef;
    _grid = grid;
  }

  bool isHud() => true;

  @override
  void render(Canvas canvas) {
    final rectSize = _grid.getRectSize();

    Paint rectPaint = Paint()..color = Colors.blueGrey;

    final viewPort = new Rect.fromLTWH(gameRef.camera.x, gameRef.camera.y,
        gameRef.size.width, gameRef.size.height);

    // Drawing x-axis
    canvas.drawRect(
        Rect.fromLTWH(0, gameRef.size.height - rectSize.height,
            gameRef.size.width, rectSize.height),
        rectPaint);
    canvas.drawLine(
        Offset(0, gameRef.size.height - rectSize.height),
        Offset(gameRef.size.width, gameRef.size.height - rectSize.height),
        Paint()
          ..color = Colors.black
          ..strokeWidth = rectSize.width / 70);

    _grid.getColumnRectsInCameraViewport(viewPort).forEach((element) {
      _drawRect(
          canvas,
          new Offset(element.x - gameRef.camera.x,
              gameRef.size.height - element.height),
          new Size(element.width, element.height),
          element.initialColor,
          (_grid.rects[0].length - element.column).toString());
    });

    // Drawing y-axis
    canvas.drawRect(
        Rect.fromLTWH(gameRef.size.width - rectSize.width, 0, rectSize.width,
            gameRef.size.height),
        rectPaint);
    canvas.drawLine(
        Offset(gameRef.size.width - rectSize.width, 0),
        Offset(gameRef.size.width - rectSize.width, gameRef.size.height),
        Paint()
          ..color = Colors.black
          ..strokeWidth = rectSize.width / 70);

    _grid.getRowRectsInCameraViewport(viewPort).forEach((element) {
      var height = element.height;
      _drawRect(
          canvas,
          new Offset(
              gameRef.size.width - element.width, element.y - gameRef.camera.y),
          new Size(element.width, height),
          element.initialColor,
          (_grid.rects.length - element.row).toString());
    });

    // Drawing right bottom corner
    var x = gameRef.size.width - rectSize.width;
    var y = gameRef.size.height - rectSize.height;
    canvas.drawRect(
        Rect.fromLTWH(x, y, rectSize.width, rectSize.height), rectPaint);
    canvas.drawLine(
        Offset(x, y),
        Offset(x, y + rectSize.height),
        Paint()
          ..color = Colors.black
          ..strokeWidth = rectSize.width / 70);
    canvas.drawLine(
        Offset(x, y),
        Offset(x + rectSize.width, y),
        Paint()
          ..color = Colors.black
          ..strokeWidth = rectSize.width / 70);
  }

  void _drawRect(
      Canvas canvas, Offset offset, Size size, Color color, String text) {
    final x = offset.dx;
    final y = offset.dy;
    final width = size.width;
    final height = size.width;

    Rect rect = Rect.fromLTWH(x, y, width, height);
    Paint rectPaint = Paint()..color = Colors.white12;
    canvas.drawRect(rect, rectPaint);

    rectPaint = Paint()
      ..color = Colors.black
      ..strokeWidth = width / 70;

    canvas.drawLine(Offset(x, y), Offset(x + width, y), rectPaint);
    canvas.drawLine(
        Offset(x, y + height), Offset(x + width, y + height), rectPaint);
    canvas.drawLine(Offset(x, y), Offset(x, y + height), rectPaint);
    canvas.drawLine(
        Offset(x + width, y), Offset(x + width, y + height), rectPaint);

    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.black,
            fontSize: height / 3,
            fontWeight: FontWeight.w400),
        text: text);
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(canvas, new Offset(x + width / 5, y + height / 3));
  }

  @override
  void update(double t) {
    // TODO: implement update
  }
}