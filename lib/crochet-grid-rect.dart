import 'dart:ui';

import 'package:flame/components/component.dart';
import 'package:flame/components/mixins/tapable.dart';
import 'package:flame/position.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class CrochetRectComponent extends PositionComponent with Tapable {
  int row;
  int column;
  Color initialColor;
  Color color;
  bool tapped = false;
  CrochetRectTapDownCallback onTap;
  Size initialSize;
  bool disableSelectionMarker;

  CrochetRectComponent(
      int row, int column, Position position, Size size, Color color) {
    this.row = row;
    this.column = column;
    this.initialSize = size;
    setByPosition(position);
    setBySize(new Position(size.width, size.height));
    this.initialColor = color;
    this.color = color;
  }

  void render(Canvas canvas) {
    Rect boxRect = Rect.fromLTWH(x, y, width, height);
    Paint boxPaint = Paint()..color = color;
    canvas.drawRect(boxRect, boxPaint);

    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = width / 70;

    canvas.drawLine(Offset(x, y), Offset(x + width, y), paint);
    canvas.drawLine(
        Offset(x, y + height), Offset(x + width, y + height), paint);
    canvas.drawLine(Offset(x, y), Offset(x, y + height), paint);
    canvas.drawLine(Offset(x + width, y), Offset(x + width, y + height), paint);

    if (tapped && !disableSelectionMarker) {
      paint = Paint()
        ..color = Colors.black
        ..strokeWidth = width / 20;
      var margin = width / 5;
      canvas.drawLine(Offset(x + margin, y + margin),
          Offset(x + width - margin, y + height - margin), paint);
      canvas.drawLine(Offset(x + width - margin, y + margin),
          Offset(x + margin, y + height - margin), paint);
    }
  }

  @override
  void update(double t) {}

  @override
  void onTapUp(TapUpDetails details) {
    tapped = !tapped;
    onTap?.call(this);
  }

  @override
  String toString() {
    return '(row: $row, column: $column, x: $x, y: $y, width: $width, height: $height)';
  }
}

typedef CrochetRectTapDownCallback = void Function(
    CrochetRectComponent rectComponent);