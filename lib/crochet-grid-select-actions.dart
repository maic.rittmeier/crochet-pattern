import 'dart:developer';
import 'dart:math' as math;
import 'dart:ui';
import 'package:flame/components/composed_component.dart';
import 'package:flame/components/component.dart';
import 'package:flame/components/mixins/tapable.dart';
import 'package:flame/game/game.dart';
import 'package:flame/position.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'crochet-grid.dart';

class CrochetGridSelectActionsComponent extends PositionComponentWithGameRef
    with Tapable, ComposedComponent {
  final double padding = 10.0;
  final CrochetGridComponent _gridComponent;
  final Rect _rectSelection;
  final List<CrochetGridSelectActionButton> actionButtons = new List();
  CrochetGridSelectActionCallback actionCallBack;

  CrochetGridSelectActionsComponent(
      Game game,
      CrochetGridComponent gridComponent,
      Rect rectSelection,
      List<CrochetGridSelectAction> actions)
      : _gridComponent = gridComponent,
        _rectSelection = rectSelection {
    gameRef = game;

    Rect actionButtonRect = Rect.zero;
    actions.forEach((element) {
      var actionButton = CrochetGridSelectActionButton(
          Position.fromOffset(actionButtonRect.topRight), element);
      actionButtons.add(actionButton);
      actionButtonRect = Rect.fromLTWH(
          actionButtonRect.left,
          actionButtonRect.top,
          actionButtonRect.width + actionButton.toRect().width,
          math.max(actionButton.toRect().height, actionButtonRect.height));
    });

    width = actionButtonRect.width;
    height = actionButtonRect.height + padding;

    x = _rectSelection.left;
    y = _rectSelection.top - height;

    actionButtons.forEach((element) {
      element.x = element.x + x;
      element.y = element.y + y;
      add(element..onTap = onActionButtonTap);
    });
  }

  @override
  void render(Canvas canvas) {
    canvas.drawRect(Rect.fromLTWH(x, y, width, height - padding),
        Paint()..color = Colors.black);

    var trianglePath = Path();
    const triangleWidth = 10;
    var pathStart = Offset(_rectSelection.left, y + height - padding);
    trianglePath.moveTo(pathStart.dx, pathStart.dy);
    trianglePath.lineTo(pathStart.dx + triangleWidth, pathStart.dy);
    trianglePath.lineTo(
        pathStart.dx + triangleWidth / 2, pathStart.dy + padding);
    trianglePath.close();
    canvas.drawPath(
        trianglePath,
        Paint()
          ..color = Colors.black
          ..style = PaintingStyle.fill);

    super.render(canvas);

    [for (var i = 0; i < actionButtons.length - 1; i += 1) i].forEach((index) {
      var buttonRect = actionButtons[index].toRect();
      canvas.drawLine(
          buttonRect.topRight,
          buttonRect.bottomRight,
          Paint()
            ..color = Colors.white
            ..strokeWidth = 1);
    });
  }

  @override
  void update(double t) {
    // TODO: implement update
  }

  void onActionButtonTap(CrochetGridSelectActionButton actionButton) {
    actionCallBack?.call(actionButton.action, _rectSelection);
  }
}

class CrochetGridSelectActionButton extends PositionComponent with Tapable {
  final double margin = 5.0;
  final CrochetGridSelectAction action;
  TextPainter _textPainter;
  CrochetGridSelectActionButtonTapDownCallback onTap;
  bool _tappedDown = false;

  CrochetGridSelectActionButton(Position position, this.action) {
    _textPainter = TextPainter(
        text: TextSpan(
            style: TextStyle(color: Colors.white, fontSize: 15),
            text: actionText),
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    _textPainter.layout();
    this.x = position.x;
    this.y = position.y;
    this.width = _textPainter.width + margin * 2;
    this.height = _textPainter.height + margin * 2;
  }

  @override
  void render(Canvas canvas) {
    canvas.drawRect(toRect(),
        Paint()..color = _tappedDown ? Colors.white38 : Colors.transparent);
    _textPainter.paint(canvas, Offset(x + margin, y + margin));
  }

  @override
  void onTapDown(TapDownDetails details) {
    _tappedDown = true;
  }

  @override
  void onTapUp(TapUpDetails details) {
    _tappedDown = false;
    onTap?.call(this);
  }

  get actionText {
    switch (action) {
      case CrochetGridSelectAction.Copy:
        return "Kopieren";
      case CrochetGridSelectAction.Paste:
        return "Einfügen";
      case CrochetGridSelectAction.Cut:
        return "Ausschneiden";
      case CrochetGridSelectAction.Delete:
        return "Löschen";
    }
  }
}

typedef CrochetGridSelectActionButtonTapDownCallback = void Function(
    CrochetGridSelectActionButton actionButton);

typedef CrochetGridSelectActionCallback = void Function(
    CrochetGridSelectAction action, Rect rectSelection);

enum CrochetGridSelectAction {
  Copy,
  Paste,
  Cut,
  Delete,
}
