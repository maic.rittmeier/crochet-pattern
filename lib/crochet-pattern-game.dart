import 'dart:developer';

import 'package:crochet_pattern/crochet-grid-ccordinates.dart';
import 'package:crochet_pattern/zoom.dart';
import 'package:flame/components/mixins/tapable.dart';
import 'package:flame/game/base_game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

import 'crochet-grid-select-actions.dart';
import 'crochet-grid.dart';

class CrochetGame extends BaseGame
    with HasTapableComponents, HasZoomableComponents, ScaleDetector {
  Offset _lastScaleLocalFocalPoint;
  double _lastScale;
  double _lastScaleEnd;
  double _zoomFactor = 1.0;
  Rect _rawSelection;
  Rect _rectSelection;
  CrochetGridComponent _grid;
  CrochetGridSelectActionsComponent _selectActionsComponent;
  List<List<bool>> _rectClipBoard;
  CrochetGridCoordinatesComponent _coordinatesComponent;

  CrochetGame() {
    this
      ..add(_grid = new CrochetGridComponent(this, 30, 15,
          rowColor: Color(0xff115500), oddRowColor: Color(0xff00ffff)))
      ..add(_coordinatesComponent =
          new CrochetGridCoordinatesComponent(this, _grid));
  }

  get isGridRectMarkerDisabled => _grid.isRectMarkerDisabled;

  set gridRectMarkerDisabled(bool disabled) {
    _grid.rectMarkerDisabled = disabled;
  }

  Color getGridRowColor() {
    return _grid.rowColor;
  }

  Color getGridOddRowColor() {
    return _grid.oddRowColor;
  }

  int getGridRows() {
    return _grid.rows;
  }

  int getGridColumns() {
    return _grid.columns;
  }

  setGridSize(int rows, int columns) {
    _grid.setGridSize(rows, columns);
    Future.delayed(const Duration(milliseconds: 500), () => optimizeViewPort());
  }

  void setGridRowColor(Color color) {
    _grid.rowColor = color;
  }

  void setGridOddRowColor(Color color) {
    _grid.oddRowColor = color;
  }

  @override
  void resize(Size size) {
    final firstResize = this.size == null;
    super.resize(size);
    if (firstResize) {
      optimizeViewPort();
    }
  }

  @override
  void onTapDown(int pointerId, TapDownDetails details) {
    var tapDownDetails = TapDownDetails(
        globalPosition: details.globalPosition,
        localPosition: _addCameraOffset(details.localPosition),
        kind: details.kind);
    if (_selectActionsComponent != null) {
      if (_selectActionsComponent
          .toRect()
          .contains(tapDownDetails.localPosition)) {
        _selectActionsComponent.handleTapDown(pointerId, tapDownDetails);
      } else {
        _clearSelection();
      }
    } else {
      super.onTapDown(pointerId, tapDownDetails);
    }
  }

  @override
  void onTapUp(int pointerId, TapUpDetails details) {
    var tapUpDetails = TapUpDetails(
        globalPosition: details.globalPosition,
        localPosition: _addCameraOffset(details.localPosition),
        kind: details.kind);
    if (_selectActionsComponent != null &&
        _selectActionsComponent.toRect().contains(tapUpDetails.localPosition)) {
      _selectActionsComponent.handleTapUp(pointerId, tapUpDetails);
    } else {
      super.onTapUp(pointerId, tapUpDetails);
    }
  }

  void onLongPressStart(LongPressStartDetails details) {
    _clearSelection();
    Vibration.vibrate(duration: 250, amplitude: 255);
    _rawSelection =
        Rect.fromLTWH(details.localPosition.dx, details.localPosition.dy, 0, 0);
    _rectSelection = _grid.computeRectSelection(_toGridRect(_rawSelection));
  }

  void onLongPressMoveUpdate(LongPressMoveUpdateDetails details) {
    _rawSelection = Rect.fromLTWH(_rawSelection.left, _rawSelection.top,
        details.offsetFromOrigin.dx, details.offsetFromOrigin.dy);
    _rectSelection = _grid.computeRectSelection(_toGridRect(_rawSelection));
  }

  void onLongPressEnd(LongPressEndDetails details) {
    _rawSelection = null;
    add(_selectActionsComponent = CrochetGridSelectActionsComponent(
        this,
        _grid,
        _rectSelection,
        _rectClipBoard != null
            ? [
                CrochetGridSelectAction.Paste,
                CrochetGridSelectAction.Copy,
                CrochetGridSelectAction.Cut,
                CrochetGridSelectAction.Delete
              ]
            : [
                CrochetGridSelectAction.Copy,
                CrochetGridSelectAction.Cut,
                CrochetGridSelectAction.Delete
              ])
      ..actionCallBack = onSelectAction);
  }

  Rect _toGridRect(Rect localRect) {
    return Rect.fromLTWH(localRect.left + camera.x, localRect.top + camera.y,
        localRect.width, localRect.height);
  }

  Rect _toLocalRect(Rect gridRect) {
    return Rect.fromLTWH(gridRect.left - camera.x, gridRect.top - camera.y,
        gridRect.width, gridRect.height);
  }

  Offset _addCameraOffset(Offset position) {
    return position != null
        ? new Offset(position.dx + camera.x, position.dy + camera.y)
        : null;
  }

  Offset _removeCameraOffset(Offset position) {
    return position != null
        ? new Offset(position.dx - camera.x, position.dy - camera.y)
        : null;
  }

  void onScaleStart(ScaleStartDetails details) {
    _lastScaleLocalFocalPoint = details.localFocalPoint;
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    if (debugMode() && _rawSelection != null) {
      canvas.drawRect(_rawSelection, Paint()..color = Colors.red);
    }
    if (_rectSelection != null) {
      canvas.drawRect(
          _toLocalRect(_rectSelection), Paint()..color = Colors.white54);
    }
  }

  void onScaleUpdate(ScaleUpdateDetails details) {
    if (_lastScaleLocalFocalPoint != null) {
      if (details.scale == 1.0) {
        moveCamera(new Position(
            _lastScaleLocalFocalPoint.dx - details.localFocalPoint.dx,
            _lastScaleLocalFocalPoint.dy - details.localFocalPoint.dy));
      } else if (details.scale != 1.0) {
        _lastScale = details.scale;
        final zoomFactor =
            _lastScaleEnd != null ? _lastScaleEnd + _lastScale : _lastScale;

        updateZoom(zoomFactor);
      }
    }
    _lastScaleLocalFocalPoint = details.localFocalPoint;
  }

  void onScaleEnd(ScaleEndDetails details) {
    _lastScaleLocalFocalPoint = null;
    _lastScaleEnd = _lastScale;
  }

  Position getCameraCenter() {
    return new Position(camera.x + size.width / 2, camera.y + size.height / 2);
  }

  void moveCamera(Position delta) {
    var currentPosition = camera;
    setCameraPosition(
        new Position(currentPosition.x + delta.x, currentPosition.y + delta.y));
  }

  void setCameraCenter(Position center) {
    setCameraPosition(
        new Position(center.x - size.width / 2, center.y - size.height / 2));
  }

  void setCameraPosition(Position position) {
//    final gridRect = _grid.toRect();
//    final cameraRect = Rect.fromLTWH(position.x, position.y,
//        this.size.width, this.size.height);
//    final limitedPosition =
//    new Position(cameraRect.right > gridRect.right ? position.x - (cameraRect.right - gridRect.right): position.x,
//        cameraRect.bottom > gridRect.bottom ? position.y - (cameraRect.bottom - gridRect.bottom): position.y);
//    camera = limitedPosition;
    camera = position;
  }

  void updateZoom(double zoomFactor) {
    if (_zoomFactor == zoomFactor) return;
    _clearSelection();
    final cameraCenter = getCameraCenter();
    var zoomDelta = (zoomFactor / _zoomFactor);
    _zoomFactor = zoomFactor;
    onZoom(_zoomFactor);
    setCameraCenter(
        new Position(cameraCenter.x * zoomDelta, cameraCenter.y * zoomDelta));
  }

  void optimizeViewPort() {
    var gridRect = _grid.toRect();
    var rectSize = _grid.getRectSize();
    setCameraPosition(new Position(
        gridRect.width - this.size.width + rectSize.width,
        gridRect.height - this.size.height + rectSize.height * 2));
  }

  void _clearSelection() {
    if (_selectActionsComponent != null) {
      markToRemove(_selectActionsComponent);
      _selectActionsComponent = null;
    }
    _rectSelection = null;
    _rawSelection = null;
  }

  void onSelectAction(CrochetGridSelectAction action, Rect rectSelection) {
    var selectedRects = _grid.getRectsBySelection(_rectSelection);
    switch (action) {
      case CrochetGridSelectAction.Copy:
        _rectClipBoard = CrochetGridComponent.rectsToBool(selectedRects);
        break;
      case CrochetGridSelectAction.Paste:
        _grid.insert(_rectClipBoard, selectedRects);
        break;
      case CrochetGridSelectAction.Cut:
        _rectClipBoard = CrochetGridComponent.rectsToBool(selectedRects);
        _grid.deleteTabbingForRects(selectedRects);
        break;
      case CrochetGridSelectAction.Delete:
        _grid.deleteTabbingForRects(selectedRects);
        break;
    }
    _clearSelection();
  }
}
