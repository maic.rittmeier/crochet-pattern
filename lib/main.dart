
import 'package:crochet_pattern/crochet-pattern-ui.dart';
import 'package:flame/flame.dart';
import 'package:flame/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:screen/screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Util().fullScreen();
  await Util().setOrientation(DeviceOrientation.portraitUp);

  CrochetPatternUI app = CrochetPatternUI();

  runApp(
    MaterialApp(
      title: 'Crochet Pattern',
      theme: ThemeData.from(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey),
      ),
      home: app,
      debugShowCheckedModeBanner: false,
    ),
  );

  Screen.keepOn(true);
  Flame.bgm.initialize();
//  Flame.bgm.play('background-music.mp3');
}
