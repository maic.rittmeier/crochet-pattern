import 'package:flame/game/base_game.dart';

mixin HasZoomableComponents on BaseGame {
  Iterable<Zoomable> get _zoomableComponents =>
      components.where((c) => c is Zoomable).cast();

  void onZoom(double zoomFactor) {
    _zoomableComponents.forEach((c) => c.handleZoomChanged(zoomFactor));
  }
}

mixin Zoomable {
  void handleZoomChanged(double zoomFactor);
}